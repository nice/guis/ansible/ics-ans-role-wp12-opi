# ics-ans-role-wp12-opi

Ansible role to install wp12-opi.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-wp12-opi
```

## License

BSD 2-clause
