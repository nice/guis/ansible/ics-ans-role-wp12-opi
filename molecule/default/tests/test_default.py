import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

files_for_distro = {"ubuntu": "/etc/skel/opisControlDashboard", "centos": "/etc/profile.d/nss_opi.sh"}


def test_files(host):
    # TODO: implement at least a test
    distro = host.system_info.distribution
    file = host.file(files_for_distro[distro])
    assert file.exists


def test_user_link(host):
    if host.system_info.distribution == "ubuntu":
        for user in host.file("/home").listdir():
            file = host.file("/home/" + user + "/opisControlDashboard")
            assert file.is_symlink
